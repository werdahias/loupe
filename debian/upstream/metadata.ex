# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/loupe/issues
# Bug-Submit: https://github.com/<user>/loupe/issues/new
# Changelog: https://github.com/<user>/loupe/blob/master/CHANGES
# Documentation: https://github.com/<user>/loupe/wiki
# Repository-Browse: https://github.com/<user>/loupe
# Repository: https://github.com/<user>/loupe.git
